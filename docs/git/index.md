---
title: "Git：安装和使用"
slug: "Git"
date: 2022-04-29T23:09:16+08:00
draft: false
---

## 简介

### 什么是Git

是一个分布式的版本控制系统，能够方便我们**管理不同版本的文件。**

例如，对某代码进行修改，但不确定修改后是否能够运行。此时可以先**提交**，再**修改**。如果修改后发现出问题，想要退回到原来版本，就可以**重置**。

> **对于Windows用户，Git还为我们提供了一个类似于Linux的命令行环境，可以方便地运行Linux命令。**

### Git和Github的关系

**Git**只**是软件**，需要你下载装到电脑上，实现git功能。

**Github、BitBucket、Gitee**基于git的项目托管平台，说白了**是云服务器或云盘**，存储分享你的代码，查看追更别人的代码。 Github、BitBucket是国外的，访问情况堪忧。

### Git和Github Desktop的关系

Github Desktop是Github开发的一款基于Git的软件。可以理解为简化版、友好版的Git。

- Git可以连接多种代码托管平台，Github Desktop只能连接Github。
- Git通常需要在命令行中手动连接Github账号，进行项目的推送或拉取等等，Github Desktop可以在图形界面里进行以上操作，就像我们登录QQ一样方便。
- Github Desktop不提供命令行界面，所有操作都在图形界面完成。涉及到比较复杂的操作，或是要运行Bash命令时无能为力。

## 下载和安装

官网：[Git - Downloads (git-scm.com)](https://git-scm.com/downloads)

> 国内直接访问可能会比较慢，~~自行解决~~。

这里还是贴一个备用链接吧。**需要注意的是，如果你现在下载git很慢，那么之后连接GitHub也大概率会出错。**

备用链接：[[软件\]soft/[开发工具]Dev - Kerm's Onemanager (stingy-tattered-bicycle.glitch.me)](https://stingy-tattered-bicycle.glitch.me/[软件]soft/[开发工具]Dev/)

![](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211202163901646.png)

Git安装时有很多步骤，里面会让你进行非常多的设置。没**有特殊情况我们就直接全部保持默认，一路`next`就好了**。

完成之后会在右键菜单里面添加`Git Bash`和`Git GUI`

![](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211202164132802.png)

## 使用说明

### 在特定文件夹建立Git仓库并备份

如上右键打开即可。

> 如果安装时选择了集成（默认选项），也可以在Windows终端中使用`Git`。

初始化并备份：

```
git init    #初始化仓库
git add .   #将所有文件添加到仓库里
git commit -m "init" #提交，附加说明“init”
```

以后再进行备份只需输入后面两条命令即可。说明可以自己写，推荐至少要让自己看得懂。

### 查看备份情况并恢复

```
git log #查看备份了哪些版本
git reset --hard 版本序列号 #恢复某版本
```

### 连接到远程仓库

> **推荐使用Github和Github Desktop**

参考文献3。

## Git Submodule

管理多个项目，其中有嵌套关系的，可以是使用`Github submodule`。

> 我接触到这个是因为使用hugo搭建博客时主题是作为`submodule`引入的。

`submodule`和主项目分开进行推送。所以在本地修改了`submodule`的文件，直接在主项目里面是看不到的，也不能推送到云端。要在`submodule`创建一个新的仓库单独推送。

详情请参考[Git中submodule的使用 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/87053283)

## .gitignore

用于设置忽略的文件。

详情参考：[Git入门之.gitignore - 掘金 (juejin.cn)](https://juejin.cn/post/6998911250323390501)

## 网络问题

在国内有时候无法直接访问Github，推荐的解决方法是寻找上网工具。

如果只是需要一个远程仓库，也可以考虑Gitee。但是上面的项目要比Github的项目少很多。

参照参考文献3。

## 附录

### 参考文献

1. [Git是什么？可以用来做什么？如何使用？ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/99313784)
2. [Git - Documentation (git-scm.com)](https://git-scm.com/doc)
3. [在VSCode中使用码云(Gitee)进行代码管理_watfe的专栏-CSDN博客](https://blog.csdn.net/watfe/article/details/79761741)

<!-- ### 版权信息

本文原载于kermsite.com，复制请保留原文出处。 -->



