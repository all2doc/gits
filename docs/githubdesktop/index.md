---
title: "Github Desktop：友好的图形化Git客户端"
slug: "Githubdesktop"
date: 2022-05-05T17:51:25+08:00
draft: false
---


## 缘起

众所周知对于程序员来说Git是一个非常重要的工具。不过对于大多数人而言Git初次上手还是略显困难。尤其是当我们部署静态网站博客时，大部分教程都会要求Git的安装和连接到云端，而这一段繁琐的过程（加上国内网络环境下Github本身访问的不稳定导致同步出错），往往会让大家提前放弃。


使用带有图形界面的`Github Desktop`，就能让这个过程不那么痛苦

## 简介

`Github Desktop`，这是Github的专用桌面端，你可以把它认为是一键绑定Github账号并且具有图形界面的，方便使用的简化版的Git。

> 当然，当你的使用进一步深入之后还是推荐原版Git。Github Desktop阉割掉了一些功能

## 下载和安装

首先访问链接下载：

- [GitHub Desktop | Simple collaboration from your desktop](https://desktop.github.com/)

![官网](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211116223530980.png)

安装完成之后，选择使用Github登录：![登录界面](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211116223525407.png)

## 使用说明

Github Desktop常见的功能就是将云端和本地的项目保持同步。这里可以选择从云端克隆项目到本地（`Clone a respository`），也可以把本地已经写好的文件推送到云端（`add local respository`）。

### 云端克隆项目到本地

![添加在线仓库](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211116223701401.png)

选择仓库，点击克隆，就会将云端代码自动复制到本地的`Local path`文件夹下面了。

进入文件夹，对文件进行修改，回到软件，如下图显示：

![同步修改](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211116231216648.png)

右侧会显示更改的详细情况（我这里有点敏感就全部涂掉了）

此时点击`Commit to main`就会在**本地**确认更改。

![本地确认](https://cdn.jsdelivr.net/gh/yw2667899/pic/img/image-20211116231329392.png)

然后再点击右上角的`Push origin`**就可以把本地的更改推送到云端了**。

如果你是使用的自动部署服务比如`netify`和`vercel`，会自动监控文件的更改，及时发布你的内容

### 本地项目推送到云端

首先用`Git`初始化一个本地项目（初始化已有项目这一点`Github Desktop`还做不到）

> `Git`参照：[Git：安装和使用](/p/git安装和使用/)

选择本地文件夹，然后一路配置即可

## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
