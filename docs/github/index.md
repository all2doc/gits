---
title: "Github"
slug: "Github"
date: 2022-04-29T23:09:20+08:00
draft: false
---

## 缘起

**可以跳过。**

为什么要写一篇这样的文章呢？

这几天知乎时不时给我推荐一些什么“电脑上有什么非装不可的软件”、“有哪些实用软件推荐”之类的文章和问题。这些文章真的是群魔乱舞，不可否认有一些确实挺有用的，但是同时很多营销号也乘机引流，说什么到某某公众号回复某某关键词即可下载。又不是你家开发的软件，非要到你的公众号下载，贱不贱呐。

而且这样的软件也不一定可靠，很有可能给你安装一个后门什么的，用起来也是非常的危险。

对于软件的下载。我一向只推荐两个渠道。

- 大型商业软件，例如Office、PS等，去官网下载
- 小工具，例如录屏、下载器、视频抓取等，去Github找相关项目。

## 简介 

### 什么是Github

GitHub 是一个


例如：

[hehonghui/the-economist-ebooks: 经济学人(含音频)、纽约客、自然、新科学人、卫报、科学美国人、连线、大西洋月刊、新闻周刊、国家地理等英语杂志免费下载、订阅(kindle推送),支持epub、mobi、pdf格式, 每周更新. The Economist 、The New Yorker 、Nature、The Atlantic 、New Scientist、The Guardian、Scientific American、Wired、Newsweek magazines, free download and subscription for kindle, mobi、epub、pdf format. (github.com)](https://github.com/hehonghui/the-economist-ebooks)


# 如何访问和合理的使用Github

### 访问Github

由于众所周知的原因，在国内访问Github比较

[Migrate from GitHub to jsDelivr](https://www.jsdelivr.com/github)

[提高国内访问 github 速度的 9 种方法！ - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/314071453)

[GitHub Proxy 代理加速 (ghproxy.com)](https://ghproxy.com/)

[https://hub.fastgit.org](https://hub.fastgit.org/)

[7ED.NET Github RAW Accelerate | raw.sevencdn.com](https://www.7ed.net/gra/)



GitHub打不开，使用开发者边车：
[dev-sidecar: 开发者边车，github打不开，github 加速, git clone加速，google CDN加速，Stack Overflow加速 (gitee.com)](https://gitee.com/docmirror/dev-sidecar)



## 附录

### 参考文献

### 版权信息

本文原载于kermsite.com，复制请保留原文出处。

