---
title: "Gitlab命令行工具"
slug: "Gitlabcli"
date: 2022-05-05T21:23:23+08:00
draft: false
---

使用 Web 界面管理 Gitlab 虽然很用户友好，但是时间长了也感觉点来点去非常麻烦。好在 Gitlab 也提供了命令行工具，可以直接使用。





GLab is an open source GitLab CLI tool bringing GitLab to your terminal next to where you are already working with `git` and your code without switching between windows and browser tabs. Work with issues, merge requests, **watch running pipelines directly from your CLI** among other features. Inspired by [gh](https://github.com/cli/cli), the official GitHub CLI tool.

`glab` is available for repositories hosted on GitLab.com and self-hosted GitLab Instances. `glab` supports multiple authenticated GitLab instances and automatically detects the authenticated hostname from the remotes available in the working git directory.

[![image](68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f34313930363132382f38383936383537332d30623535363430302d643239662d313165612d383530342d3865636439633239323236332e706e67.png)](https://user-content.gitlab-static.net/9e259791528f0dd99ccde4793aa569f289aef7b7/68747470733a2f2f757365722d696d616765732e67697468756275736572636f6e74656e742e636f6d2f34313930363132382f38383936383537332d30623535363430302d643239662d313165612d383530342d3865636439633239323236332e706e67)

## Usage

```shell
glab <command> <subcommand> [flags]
```

## Documentation

Read the [documentation](https://glab.readthedocs.io/) for usage instructions.

## Installation

### Quick Install

**Supported Platforms**: Linux and macOS

#### Homebrew

```shell
brew install glab
```

Updating (Homebrew):

```shell
brew upgrade glab
```

### Windows

Available for download via [WinGet](https://github.com/microsoft/winget-cli), [scoop](https://scoop.sh/), or downloadable EXE installer file.

#### Scoop

```powershell
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser # Optional: Needed to run a remote script the first time
Invoke-WebRequest get.scoop.sh | Invoke-Expression
```

```shell
scoop install glab
```

Updating (Scoop):

```shell
scoop update glab
```

### snap

Make sure you have [snap installed on your Linux Distro](https://snapcraft.io/docs/installing-snapd).

1. `sudo snap install --edge glab`
2. `sudo snap connect glab:ssh-keys` to grant ssh access

## Authentication

Get a GitLab access token at https://gitlab.com/-/profile/personal_access_tokens or https://gitlab.example.com/-/profile/personal_access_tokens if self-hosted

- start interactive setup

  ```shell
  glab auth login
  ```

- authenticate with token and hostname (Not recommended for shared environments)

  ```shell
  glab auth login --hostname gitlab.example.org --token xxxxx
  ```

## Configuration

By default, `glab` follows the XDG Base Directory [Spec](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html): global configuration file is saved at `~/.config/glab-cli`. Local configuration file is saved at `.git/glab-cli` in the current working git directory. Advanced workflows may override the location of the global configuration by setting the `GLAB_CONFIG_DIR` environment variable.

**To set configuration globally**

```shell
glab config set --global editor vim
```

**To set configuration for current directory (must be a git repository)**

```shell
glab config set editor vim
```

**To set configuration for a specific host**

Use the `--host` flag to set configuration for a specific host. This is always stored in the global config file with or without the `global` flag.

```shell
glab config set editor vim --host gitlab.example.org
```

## 使用说明

[GLab - A GitLab CLI Tool — GLab documentation](https://glab.readthedocs.io/en/latest/index.html)


## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
