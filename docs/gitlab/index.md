---
title: "Gitlab"
slug: "Gitlab"
date: 2022-05-05T17:30:49+08:00
draft: false
---

官网：[Getting Title at 32:18](https://gitlab.com/)

相较于 Github，Gitlab 有以下优势：

- 国内可以直接访问
- WebIDE 支持
- Gitlab runner
- 支持自托管（Selfhost）

缺点：

- 社区环境较 Github 差。
- 部分服务不支持 Gitlab

## Gitlab 入门

在 Gitlab 中，我们直接创建的是 Project，一个 Proje 可以包含仓库、CI/CD 等内容。

<!-- ## Gitlab 团队管理

您可以创建多个团队。可以使用 Group -->


只有添加 Owner 权限，其他人才可以直接在 main 分支提交修改。

## 权限

注意：Private Group 内的所有仓库不可设置 Public。Public Group 内的所有仓库不可设置 Private。

相应选项会变成灰色。

请注意父子级之间的权限关系。



## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
