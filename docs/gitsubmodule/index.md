---
title: "Gitsubmodule"
slug: "Gitsubmodule"
date: 2022-05-05T17:53:41+08:00
draft: false
---

GIT 的一个很大的问题是没有权限划分，所有人对项目下所有东西都有（查看）权限（只能设置分支的推送权限），无法对特定文件和文件夹设置单独的权限。这个功能只能借用 SUBMODULE 来实现。

1、添加 SUBMODULE
git submodule add https://github.com/xxx/yyy sub/module
如果之前已经克隆了项目到指定位置，再执行git submodule add也是可以的，不会有冲突。添加后能在项目根目录下看到.gitmodules文件，里面内容为：

[submodule "sub/module"]
path = sub/module 
url = https://github.com/xxx/yyy 
如果添加过多个子项目，该文件里就会有多个条目。

还有一个变化是，.git/modules下面会添加项目的缓存。

2、移动 SUBMODULE 的位置
直接用git mv命令即可：

git mv dir1/sub1 dir2/sub2
3、删除 SUBMODULE
GIT 未提供submodule remove的功能。要删除一个子模块，需按照下面步骤操作：

git submodule deinit sub/module，执行后模块目录将被清空。
git rm sub/module，执行后会清除.gitmodules里的项目。
git commit -m 'remove sub/module。
第一步不做似乎也没关系。第二步是关键，这里不能直接rm sub/module，这样不会同步更新.gitmodules的内容。

4、克隆带 SUBMODULE 的项目
克隆母项目不会自动下载所包含的子项目，必须克隆之后再执行submodule init & update：

git clone /path/to/repos/foo.git
git submodule init & update
如果子项目里还有子项目，可以用--recursive一次性搞定：

git submodule init & update --recursive
还有一个更简单的方法：

git clone --recursive /path/to/repos/foo.git
5、更新带 SUBMODULE 的项目
如果别人更新了母项目所指向的子项目的分支，此时单纯的拉取和合并母项目，并不会自动更新本地的子项目（通过 git status 能看到具体信息），必须执行submodule update：

git pull origin master
git status
git submodule update
注意git submodule update也会覆盖本地对子项目指向的操作。因此需要注意当出现子项目new commits时，是因为其它人更改过还是自己对子项目更改过。

如果母项目添加了新的子模块，还需要像上面一样先init：

git pull origin master
git status
git submodule init
git submodule update

```bash
git submodule add git://github.com/CaiJimmy/hugo-theme-stack/ themes/hugo-theme-stack
```

stack主题不能在vercel部署，但是可以在netlify部署。

使用vercel自带的内容，然后使用上面的命令下载theme，然后同步就可以成功了。

```
git submodule add git://github.com/adityatelange/hugo-PaperMod.git themes/PaperMod
```

使用Git的注意事项：

空文件夹不会被上传。

所以看到上传后文件突然少了好多，并不是出了什么问题。

```
git init 
git add .
```

[开始使用 | Hugo 主题 Stack (jimmycai.com)](https://docs.stack.jimmycai.com/zh/getting-started.html)



**最低 Hugo 版本要求为 0.78.0。**

以下情况需要用到 Hugo Extended 版本：

- 修改 SCSS 文件
- 或使用 GitHub 仓库的最新版本


使用submodule构建和管理嵌套项目。

为了更好地展示文档的开发进程，我希望能够通过Hugo管理Mkdocs，即在Hugo中content文件夹下放置Mkdocs的仓库。





其次解决仓库嵌套的问题。我希望子文件夹本身是一个仓库，可以直接部署到vercel，而父文件夹也是一个仓库，也能够部署，并且包含子文件夹的所有内容。

```
git submodule add https://github.com/chaconinc/DbConnector 
```

指定文件目录：

```
$ git submodule add https://github.com/LukasJoswiak/etch.git themes/etch
```

其中，themes是已经存在的文件夹，etch不存在的、新建的目录，此命令会将仓库中所有文件直接复制到etch中。

作用类似git clone，但是他会在父仓库的下面新建.gitmodules文件，并且包含以下内容

```
[submodule "apps/firstApp"]
	path = apps/firstApp
	url = https://github.com/muchang/mean-seed-app.git
```

这一段表示子仓库的位置，以及子仓库的远程仓库的地址。

[Git - Submodules (git-scm.com)](https://git-scm.com/book/en/v2/Git-Tools-Submodules)

[Git中submodule的使用 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/87053283)





为了能在远端部署含有private 仓库的submodule，需要配置deploy key：

[Repository permissions and linking | Netlify Docs](https://docs.netlify.com/configure-builds/repo-permissions-linking/)

### Git submodules

To include an outside repository as a subdirectory in your own repository, *always configure it as a [submodule](https://blog.github.com/2016-02-01-working-with-submodules/)*. If you do not, it may work locally using cloning, but the sub-repository content will not be pushed to your Git provider, and it will not be available to your build on Netlify.

If a submodule repository is *public*, you can use `https` format (for example, `https://github.com/owner/project`) to link it in your submodule configuration. If the repository is *private*, or if you prefer to use `ssh` format (for example, `git@github.com:owner/project.git`), you will need to follow the instructions below to generate a deploy key in Netlify and add it to the submodule repository settings.

#### [#](https://docs.netlify.com/configure-builds/repo-permissions-linking/#deploy-keys)Deploy keys

Submodule repositories linked in `ssh` format, *including all private submodule repositories*, require an SSH key called a **deploy key** ([in GitHub](https://developer.github.com/v3/guides/managing-deploy-keys/#deploy-keys) and [in GitLab](https://docs.gitlab.com/ee/user/project/deploy_keys/index.html)) or **access key** ([in Bitbucket](https://confluence.atlassian.com/bitbucketserver/ssh-access-keys-for-system-use-776639781.html)).

SSH keys are actually a pair of keys: one private, and one public. To generate a new key pair for a Netlify site, go to **Site settings > Build & deploy > Continuous deployment > Deploy key**, and select **Generate public key**. Netlify will store the private key, and provide the public key for you to add to the repository settings for your submodule.

![img](https://d33wubrfki0l68.cloudfront.net/3a679ea4c8942beaca218660a07cc45821c608d5/5fcef/images/configure-builds-deploy-key-existing.png)



[Deploy keys | GitLab](https://docs.gitlab.com/ee/user/project/deploy_keys/index.html)

![image-20220427112046997](C:\Users\yysf1\AppData\Roaming\Typora\typora-user-images\image-20220427112046997.png)

## Create a project deploy key

Prerequisites:

- You must have at least the Maintainer role for the project.
- [Generate an SSH key pair](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair). Put the private SSH key on the host that requires access to the repository.

1. On the top bar, select **Menu > Projects** and find your project.
2. On the left sidebar, select **Settings > Repository**.
3. Expand **Deploy keys**.
4. Complete the fields.
5. Optional. To grant `read-write` permission, select the **Grant write permissions to this key** checkbox.

A project deploy key is enabled when it is created. You can modify only a project deploy key’s name and permissions.

## 附录

### Reference

### 版权声明

本文原载于https://all2doc.com
